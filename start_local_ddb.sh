#!/bin/bash

sudo docker run \
-v /docker_vols/ddb:/docker_vols/ddb/ \
-p 8000:8000 \
dwmkerr/dynamodb \
-dbPath /docker_vols/ddb/
