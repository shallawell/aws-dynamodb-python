#!/usr/bin/python
from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
import sys

# to do add usage statement and test for args
# see https://docs.python.org/2.7/library/argparse.html#usage

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource("dynamodb", region_name='us-west-2', endpoint_url="http://localhost:8000")

table = dynamodb.Table('Movies')


# print get title from cmd line arg
#print("Arg1 is:", sys.argv[1])
#title = "Fame"
#title = "Friday the 13th"
title = sys.argv[1]
# print get year from cmd line arg
#print("Arg2 is:", sys.argv[2])
#year = 1980
year = int(sys.argv[2])

try:
    response = table.get_item(
        Key={
            'year': year,
            'title': title
        },
# this is how to filter to specific attributes
        ProjectionExpression="#yr, title, info.actors[0]",
        ExpressionAttributeNames={ "#yr": "year" }, # Expression Attribute Names for Projection Expression only.
    )
except ClientError as e:
    print(e.response['Error']['Message'])
else:
    item = response['Item']
    print("GetItem succeeded:")
# using response gets the request meta-data, whereas item just gets the 'db record'
#    print(json.dumps(item, indent=4, cls=DecimalEncoder))
#    print(json.dumps(response, indent=4, cls=DecimalEncoder))

#print("year:", year)
#print("title:", title)
#print("item:", item)
