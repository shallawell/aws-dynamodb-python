#!/usr/bin/python
from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")

table = dynamodb.Table('Movies')

year = 1980

# quick count 
response = table.scan(
    FilterExpression=Attr('year').eq(year),
    Select='COUNT',
# ensure valid data, just good practice
    ConsistentRead=True
)
print("Movies from ", year)

#print(response)
print(json.dumps(response, indent=4, cls=DecimalEncoder))
