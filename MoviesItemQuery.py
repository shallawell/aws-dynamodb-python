#!/usr/bin/python
from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")

table = dynamodb.Table('Movies')

year = 1980
#title = ['title']
#info = ['info']

# query 
response = table.query(
#    ProjectionExpression="#yr, title, info.genres, info.actors[0]",
    ProjectionExpression="#yr, title, info.rating",
    ExpressionAttributeNames={ "#yr": "year" }, # Expression Attribute Names for Projection Expression only.
    KeyConditionExpression=Key('year').eq(year) & Key('title').between('A', 'F'),
#    KeyConditionExpression=Key('year').eq(year),
# ensure valid data, just good practice
    ConsistentRead=True
)
print("Movies from ", year)

#print("response")
#print(response)

#print("responseItems")
#print(response['Items'])

#print("GetItem succeeded:")
#print(json.dumps(response, indent=4, cls=DecimalEncoder))

for i in response['Items']:
#    print(i['info'])
#    print(i['year'], ":", i['title'])
    print(i['year'], ":", i['title'], ":", i['info'])
